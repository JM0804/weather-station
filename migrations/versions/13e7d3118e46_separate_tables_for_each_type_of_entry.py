"""Separate tables for each type of entry

Revision ID: 13e7d3118e46
Revises: 1a8030629b5f
Create Date: 2019-03-29 02:09:48.248425

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '13e7d3118e46'
down_revision = '1a8030629b5f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('gas_resistance_entry',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datetime', sa.TIMESTAMP(timezone=True), nullable=True),
    sa.Column('entry', sa.Numeric(precision=2), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('pressure_entry',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datetime', sa.TIMESTAMP(timezone=True), nullable=True),
    sa.Column('entry', sa.Numeric(precision=1), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('relative_humidity_entry',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datetime', sa.TIMESTAMP(timezone=True), nullable=True),
    sa.Column('entry', sa.Numeric(precision=1), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('temperature_entry',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datetime', sa.TIMESTAMP(timezone=True), nullable=True),
    sa.Column('entry', sa.Numeric(precision=1), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('uv_index_entry',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datetime', sa.TIMESTAMP(timezone=True), nullable=True),
    sa.Column('entry', sa.Numeric(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_table('entry')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('entry',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('datetime', sa.TIMESTAMP(), nullable=True),
    sa.Column('temperature', sa.NUMERIC(precision=1), nullable=True),
    sa.Column('gas_resistance', sa.NUMERIC(precision=2), nullable=True),
    sa.Column('pressure', sa.NUMERIC(precision=1), nullable=True),
    sa.Column('relative_humidity', sa.NUMERIC(precision=1), nullable=True),
    sa.Column('uv_index', sa.NUMERIC(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_table('uv_index_entry')
    op.drop_table('temperature_entry')
    op.drop_table('relative_humidity_entry')
    op.drop_table('pressure_entry')
    op.drop_table('gas_resistance_entry')
    # ### end Alembic commands ###
