#!/bin/sh
# Weather Station Web GUI
touch /etc/systemd/system/weather-station.service
cat """[Unit]
Description=uWSGI instance to serve Weather Station web interface
After=network.target

[Service]
User=pi
Group=www-data
WorkingDirectory=/home/pi/weather-station
Environment=\"PATH=/home/pi/weather-station/.venv/bin\"
ExecStart=/home/pi/weather-station/.venv/bin/uwsgi --ini weather-station.ini

[Install]
WantedBy=multi-user.target
""" > /etc/systemd/system/weather-station.service

systemctl enable weather-station

# Weather Station Scheduler Backend
touch /etc/systemd/system/weather-station-scheduler.service
cat """[Unit]
Description=APScheduler backend for Weather Station

[Service]
User=pi
Group=www-data
WorkingDirectory=/home/pi/weather-station
Environment=\"PATH=/home/pi/weather-station/.venv/bin\"
ExecStart=/home/pi/weather-station/.venv/bin/python3 scheduler.py

[Install]
WantedBy=multi-user.target
""" > /etc/systemd/system/weather-station-scheduler.service

systemctl enable weather-station-scheduler

# Install required packages
apt install nginx python3-pip python3-venv -y

# Add Weather Station Web GUI to Nginx and enable the site
touch /etc/nginx/sites-available/weather-station
cat """server {
	listen 80;
	server_name 0.0.0.0;

	location / {
		include uwsgi_params;
		uwsgi_pass unix:///home/pi/weather-station/weather-station.sock;
	}
}
""" > /etc/nginx/sites-available/weather-station

ln -s /etc/nginx/sites-available/weather-station /etc/nginx/sites-enabled

rm /etc/nginx/sites-enabled/default

systemctl restart nginx

# Set up Python venv
python3 -m venv .venv
source .venv/bin/activate

# Install pip requirements
pip3 install wheel -y
pip3 install -r requirements.txt

# Initialise and upgrade database
flask db init
flask db upgrade

# Start Scheduler and Web GUI services
systemctl start weather-station-scheduler
systemctl start weather-station
