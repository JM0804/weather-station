print("Loading modules...")
from apscheduler.schedulers.blocking import BlockingScheduler
from app import db
from app.models import *
import datetime
import configparser
# BME680
import bme680
# VEML6075
import board
import busio
import adafruit_veml6075
# SEN-08942
import statistics
import sen08942

print("Loading config...")
config = configparser.ConfigParser()
config.read('config.ini')

# BME680
def get_temperature():
	sensor = bme680.BME680()
	sensor.set_temperature_oversample(bme680.OS_8X)
	sensor.set_filter(bme680.FILTER_SIZE_3)

	if sensor.get_sensor_data():
		t = TemperatureEntry(datetime=datetime.datetime.utcnow(), entry=sensor.data.temperature)
		db.session.add(t)
		db.session.commit()
	return

def get_relative_humidity():
	sensor = bme680.BME680()
	sensor.set_humidity_oversample(bme680.OS_2X)
	sensor.set_filter(bme680.FILTER_SIZE_3)

	if sensor.get_sensor_data():
		r = RelativeHumidityEntry(datetime=datetime.datetime.utcnow(), entry=sensor.data.humidity)
		db.session.add(r)
		db.session.commit()
	return

def get_pressure():
	sensor = bme680.BME680()
	sensor.set_pressure_oversample(bme680.OS_4X)
	sensor.set_filter(bme680.FILTER_SIZE_3)

	if sensor.get_sensor_data():
		p = PressureEntry(datetime=datetime.datetime.utcnow(), entry=sensor.data.pressure)
		db.session.add(p)
		db.session.commit()
	return

def get_gas_resistance():
	sensor = bme680.BME680()
	sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)
	sensor.set_gas_heater_temperature(config['advanced'].getint('bme680_gas_heater_temperature'))
	sensor.set_gas_heater_duration(config['advanced'].getint('bme680_gas_heater_duration'))
	sensor.select_gas_heater_profile(config['advanced'].getint('bme680_gas_heater_profile'))

	if sensor.get_sensor_data() and sensor.data.heat_stable:
		g = GasResistanceEntry(datetime=datetime.datetime.utcnow(), entry=sensor.data.gas_resistance)
		db.session.add(g)
		db.session.commit(g)
	return

# VEML6075
def get_uv_index():
	i2c = busio.I2C(board.SCL, board.SDA)
	veml = adafruit_veml6075.VEML6075(i2c, integration_time=100)

	u = UVIndexEntry(datetime=datetime.datetime.utcnow(), entry=veml.uv_index)
	db.session.add(u)
	db.session.commit()


# SEN-08942
sen08942 = sen08942.SEN08942(
	config['intermediate'].getint('sen08942_anemometer_pin'),
	config['intermediate'].getint('sen08942_bucket_pin')
)
anemometer_readings_wind = []
anemometer_readings_gust = []
def take_anemometer_reading():
	speed = sen08942.get_current_wind_speed()
	anemometer_readings_wind.append(speed)
	anemometer_readings_gust.append(speed)

def get_gust_speed():
	if len(anemometer_readings_gust) > 0:
		gust_speed = max(anemometer_readings_gust)
		anemometer_readings_gust.clear()
		g = GustSpeedEntry(datetime=datetime.datetime.utcnow(), entry=gust_speed)
		db.session.add(g)
		db.session.commit()

def get_wind_speed():
	if len(anemometer_readings_wind) > 0:
		wind_speed = statistics.mean(anemometer_readings_wind)
		anemometer_readings_wind.clear()
		w = WindSpeedEntry(datetime=datetime.datetime.utcnow(), entry=wind_speed)
		db.session.add(w)
		db.session.commit()

def get_rainfall():
	rainfall = sen08942.get_current_rainfall()
	r = RainfallEntry(datetime=datetime.datetime.utcnow(), entry=rainfall)
	db.session.add(r)
	db.session.commit()

# SCHEDULE
if __name__ == "__main__":
	print("Starting scheduler...")
	scheduler = BlockingScheduler()

	scheduler.add_job(
		func=get_temperature,
		trigger='interval',
		id='get_temperature',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('temperature_tick')
	)

	scheduler.add_job(
		func=get_pressure,
		trigger='interval',
		id='get_pressure',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('pressure_tick')
	)

	scheduler.add_job(
		func=get_relative_humidity,
		trigger='interval',
		id='get_relative_humidity',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('relative_humidity_tick')
	)

	scheduler.add_job(
		func=get_gas_resistance,
		trigger='interval',
		id='get_gas_resistance',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('gas_resistance_tick')
	)

	scheduler.add_job(
		func=get_uv_index,
		trigger='interval',
		id='get_uv_index',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('uv_index_tick')
	)

	scheduler.add_job(
		func=take_anemometer_reading,
		trigger='interval',
		id='take_anemometer_reading',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('anemometer_reading_tick')
	)

	scheduler.add_job(
		func=get_wind_speed,
		trigger='interval',
		id='get_wind_speed',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('wind_speed_tick')
	)

	scheduler.add_job(
		func=get_gust_speed,
		trigger='interval',
		id='get_gust_speed',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('gust_speed_tick')
	)

	scheduler.add_job(
		func=get_rainfall,
		trigger='interval',
		id='get_rainfall',
		next_run_time=datetime.datetime.now(),
		seconds=config['intermediate'].getint('rainfall_tick')
	)

	print("Ready!")
	scheduler.start()
