from gpiozero import Button
import datetime


class SEN08942:
	SPIN_SPEED = 2.4 # Speed required for one spin per second (in km/h)
	KMPH_TO_MPH_CONVERSION = 0.6213712 # Factor to multiply by to convert km/h to mph
	ADJUSTMENT = 1.18 # Adjustment for anemometer factor (in km/h)
	BUCKET_CAPACITY = 0.2794 # Amount of water the rain bucket can hold before it empties (in mm)

	wind_click_count = 0
	last_time = datetime.datetime.now()
	last_click_time = datetime.datetime.now()
	last_wind_speed = 0
	spun_since_last_check = False
	bucket_click_count = 0

	def __init__(self, windPin, rainPin):
		""" Initialise the SEN-08942 """
		self.wind_speed_sensor = Button(windPin)
		self.wind_speed_sensor.when_pressed = self.__spin
		self.last_time = datetime.datetime.now()
		self.rainfall_sensor = Button(rainPin)
		self.rainfall_sensor.when_pressed = self.__empty

	def __spin(self):
		""" Record the actual wind speed and increment the click counter """
		# Every click (i.e. every half rotation), take the time delta and calculate the wind speed from it
		this_time = datetime.datetime.now()
		delta_time = int((this_time - self.last_click_time).total_seconds() * 1000)
		self.last_click_time = this_time
		self.last_wind_speed = ((self.SPIN_SPEED/2)/(delta_time/1000))+self.ADJUSTMENT
		self.spun_since_last_check = True
		self.wind_click_count += 1

	def __empty(self):
		""" Increment rainfall counter """
		self.bucket_click_count += 1

	def __dt(self):
		""" Return the amount of seconds that have passed since the last check """
		this_time = datetime.datetime.now()
		delta_time = this_time - self.last_time
		self.last_time = this_time
		return delta_time.seconds

	def get_avg_wind_speed(self):
		""" Return the average wind speed based on the amount of rotations since the last check """
		interval = self.__dt()
		if interval > 0:
			clicks_per_second = self.wind_click_count/interval
			rotations_per_second = clicks_per_second/2
			wind_speed = (rotations_per_second*self.SPIN_SPEED)
			if wind_speed > 0:
				wind_speed += self.ADJUSTMENT
			self.wind_click_count = 0
			return wind_speed
		else:
			return -1

	def get_current_wind_speed(self):
		""" Return the last actual wind speed """
		return_val = 0

		if self.spun_since_last_check:
			return_val = self.last_wind_speed

		self.spun_since_last_check = False

		return return_val

	def get_current_rainfall(self):
		""" Return the amount of rain that has fallen since the last check """
		rainfall = self.bucket_click_count*self.BUCKET_CAPACITY
		self.bucket_click_count = 0
		return rainfall

if __name__ == '__main__':
	from time import sleep
	sen = SEN08942(5, 6)
	while True:
		sleep(5)
		print("avg wind: %.2f km/h" % sen.get_avg_wind_speed())
		print("cur wind: %.2f km/h" % sen.get_current_wind_speed())
		print("rainfall: %.2f mm" % sen.get_current_rainfall())
