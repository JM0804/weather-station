from flask import render_template
from app import app, config
from app.models import *
from psypy import psySI
from decimal import Decimal
from meteocalc import Temp, dew_point, heat_index

@app.route('/')
def index():
	name = config['basic']['name']

	gas_resistance = GasResistanceEntry.query.order_by(GasResistanceEntry.datetime.desc()).first()
	pressure = PressureEntry.query.order_by(PressureEntry.datetime.desc()).first()
	relative_humidity = RelativeHumidityEntry.query.order_by(RelativeHumidityEntry.datetime.desc()).first()
	temperature = TemperatureEntry.query.order_by(TemperatureEntry.datetime.desc()).first()
	uv_index = UVIndexEntry.query.order_by(UVIndexEntry.datetime.desc()).first()
	wind_speed = WindSpeedEntry.query.order_by(WindSpeedEntry.datetime.desc()).first()
	gust_speed = GustSpeedEntry.query.order_by(GustSpeedEntry.datetime.desc()).first()
	rainfall = RainfallEntry.query.order_by(RainfallEntry.datetime.desc()).first()

	# Dew point and heat index reading and formatting
	t = Temp(temperature.entry, 'c')
	dp = dew_point(temperature=t, humidity=float(relative_humidity.entry))
	dp = "{0:.2f}°C".format(dp.c)
	hi = heat_index(temperature=t, humidity=float(relative_humidity.entry))
	hi = "{0:.2f}°C".format(hi.c)

	# Wet bulb temperature reading and formatting
	wet_bulb_temperature = "?"
	if temperature and relative_humidity and pressure:
		wbt = psySI.state("DBT", float(temperature.entry+Decimal(273.15)), "RH", float((relative_humidity.entry/Decimal(100))), float(pressure.entry))[5]
		wbt-=273.15
		print(wbt)
		wet_bulb_temperature = "{0:.2f}°C".format(wbt)

	# Format gas resistance reading
	if gas_resistance:
		gas_resistance = "{0:.2f} Ohms".format(gas_resistance.entry)
	else:
		gas_resistance = "?"

	# Format pressure reading
	if pressure:
		pressure = "{0:.2f} hPa".format(pressure.entry)
	else:
		pressure = "?"

	# Format relative humidity reading
	if relative_humidity:
		relative_humidity = "{0:.2f}% RH".format(relative_humidity.entry)
	else:
		relative_humidity = "?"

	# Format temperature reading
	if temperature:
		temperature = "{0:.2f}°C".format(temperature.entry)
	else:
		temperature = "?"

	# Format UV index reading
	if uv_index:
		uv_index = "{0:.0f}".format(uv_index.entry)
		# Remove - symbol if UV index is 0:
		if uv_index == "-0":
			uv_index = "0"
	else:
		uv_index = "?"

	# Format wind speed reading
	if wind_speed:
		wind_speed = "{0:.1f} km/h".format(wind_speed.entry)
	else:
		wind_speed = "?"

	# Format gust speed reading
	if gust_speed:
		gust_speed = "{0:.1f} km/h".format(gust_speed.entry)
	else:
		gust_speed = "?"

	# Format rainfall reading
	if rainfall:
		rainfall = "{0:.1f} mm".format(rainfall.entry)
	else:
		rainfall = "?"

	return render_template(
		'index.html',
		name=name,
		gas_resistance=gas_resistance,
		pressure=pressure,
		relative_humidity=relative_humidity,
		temperature=temperature,
		uv_index=uv_index,
		wet_bulb_temperature=wet_bulb_temperature,
		dew_point=dp,
		heat_index=hi,
		wind_speed=wind_speed,
		gust_speed=gust_speed,
		rainfall=rainfall
	)
