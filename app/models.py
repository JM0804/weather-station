from app import db

class TemperatureEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric(precision=1))

	def __repr__(self):
		return 'ID: {}, DateTime: {}, Temp: {}'.format(self.id, self.datetime, self.entry)

class RelativeHumidityEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric(precision=1))

	def __repr__(self):
		return 'ID: {}, DateTime: {}, RH: {}'.format(self.id, self.datetime, self.entry)

class PressureEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric(precision=1))

	def __repr__(self):
		return 'ID: {}, DateTime: {}, Pressure: {}'.format(self.id, self.datetime, self.entry)

class GasResistanceEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric(precision=2))

	def __repr__(self):
		return 'ID: {}, DateTime: {}, Gas Resistance: {}'.format(self.id, self.datetime, self.entry)

class UVIndexEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric)

	def __repr__(self):
		return 'ID: {}, DateTime: {}, UV Index: {}'.format(self.id, self.datetime, self.entry)

class WindSpeedEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric)

	def __repr__(self):
		return 'ID: {}, DateTime: {}, Wind Speed: {}'.format(self.id, self.datetime, self.entry)

class GustSpeedEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric)

	def __repr__(self):
		return 'ID: {}, DateTime: {}, Gust Speed: {}'.format(self.id, self.datetime, self.entry)

class RainfallEntry(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	datetime = db.Column(db.TIMESTAMP(timezone=True))
	entry = db.Column(db.Numeric)

	def __repr__(self):
		return 'ID: {}, DateTime: {}, Rainfall: {}'.format(self.id, self.datetime, self.entry)
